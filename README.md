# PhoneNumber

**本项目是基于开源项目PhoneNumber进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/xdtianyu/PhoneNumber ）追踪到原项目版本**

#### 项目介绍

- 项目名称：PhoneNumber
- 所属系列：ohos的第三方组件适配移植
- 功能：一个获取号码归属地和其他信息（诈骗、骚扰等）的开源库。
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/xdtianyu/PhoneNumber
- 原项目基线版本：v0.7.17
- 编程语言：Java
- 外部库依赖：无

#### 展示效果

![avatar](screenshot/preview.gif)

#### 安装教程

##### 方案一：

1. 编译组件har包PhoneNumber.har。
2. 启动 DevEco Studio，将har包导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

##### 方案二:

  1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
 repositories {
     maven {
         url 'http://106.15.92.248:8081/repository/Releases/' 
     }
 }
```

  2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
 dependencies {
     implementation 'org.xdty.phone.number:PhoneNumber:1.0.0'
 }
```

#### 使用说明
1.代码初始化：
```
      mRxPhoneNumber = new RxPhoneNumber();
      mRxPhoneNumber.init(this);
```

2.实现函数：
```
    mRxPhoneNumber.getNumber("95194049").subscribe(new Consumer<INumber>() {
            @Override
            public void accept(INumber number) throws Exception {
                try {
                    String s = "onResponse: " +
                            number.getNumber() +
                            ": " +
                            number.getType().getText() +
                            ", " +
                            number.getName() +
                            ", " +
                            number.getCount() +
                            " : " +
                            number.getProvince() +
                            " " +
                            number.getCity() +
                            " " +
                            number.getProvider() +
                            ", " +
                            number.getApiId();
                    result += s + "\n";
                    textView.setText(result);
                    HiLog.error(Utils.TAG, (System.currentTimeMillis() - start) + ": " + s);
                } catch (Exception e) {
                    e.printStackTrace();
                }
```

#### 版本迭代

- v1.0.0
- 实现了以下功能：
  1. 号码归属地查询；
  2. 号码类型展示，包括骚扰电话，诈骗电话等。

#### 版权和许可信息

```
   Apache License Version 2.0
```


