package org.xdty.phone.number.model.mvno;


import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.model.NumberHandler;
import org.xdty.phone.number.util.Utils;

import java.io.File;

public class MvnoHandler implements NumberHandler<MvnoNumber> {

    public final static String MVNP_VERSION_CODE_KEY = "mvnp_db_version_code_key";
    public final static int MVNP_VERSION_CODE = 1;
    public final static String DB_NAME = "mvnp.db";

    private Context mContext;

    public MvnoHandler(Context context) {
        mContext = context.getApplicationContext();
        //checkVersion();
    }

    private void checkVersion() {
        DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
        Preferences pref = databaseHelper.getPreferences(mContext.getPreferencesDir().getName());
        if (pref.getInt(MVNP_VERSION_CODE_KEY, 0) < MVNP_VERSION_CODE) {
            Utils.removeCacheFile(mContext, DB_NAME);
        }
        pref.putInt(MVNP_VERSION_CODE_KEY, MVNP_VERSION_CODE);
    }

    @Override
    public String url() {
        return null;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public MvnoNumber find(String number) {
        number = Utils.fixNumberPlus(number);

        if (number.length() != 11 || (!number.startsWith("170") && !number.startsWith("171"))) {
            return null;
        }

        MvnoNumber mvnoNumber = null;

        RdbStore db = null;
        ResultSet cur = null;
        try {
            StoreConfig config = StoreConfig.newDefaultConfig(DB_NAME);
            DatabaseHelper helper = new DatabaseHelper(mContext);
            RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {

                @Override
                public void onCreate(RdbStore rdbStore) {
                    HiLog.info(Utils.TAG, "Create db" + rdbStore.toString());
                }

                @Override
                public void onUpgrade(RdbStore rdbStore, int i, int i1) {

                }
            };
            db = helper.getRdbStore(config, 1, rdbOpenCallback);

            cur = db.querySql(
                    "SELECT number.number, province.province, city.city, provider.provider "
                            + "FROM number "
                            + "JOIN province "
                            + "ON province.province_id = number.province "
                            + "JOIN city "
                            + "ON city.city_id = number.city "
                            + "JOIN provider "
                            + "ON provider.provider_id = number.provider "
                            + "WHERE number.number = ?",
                    new String[] { number.substring(2, 7) });

            if (cur.getRowCount() == 1 && cur.goToFirstRow()) {
                String province = cur.getString(cur.getColumnIndexForName("province"));
                String city = cur.getString(cur.getColumnIndexForName("city"));
                String provider = cur.getString(cur.getColumnIndexForName("provider"));
                mvnoNumber = new MvnoNumber(number, province, city, provider);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cur != null) {
                    cur.close();
                }
                if (db != null) {
                    db.close();
                }
            } catch (Exception e) {
                // ignore
            }
        }

        return mvnoNumber;
    }

    @Override
    public boolean isOnline() {
        return false;
    }

    @Override
    public int getApiId() {
        return INumber.API_ID_MVNP;
    }
}
