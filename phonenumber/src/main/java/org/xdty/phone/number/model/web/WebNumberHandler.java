package org.xdty.phone.number.model.web;

import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.model.NumberHandler;
import org.xdty.phone.number.util.Utils;

import java.io.IOException;

public class WebNumberHandler implements NumberHandler<WebNumber> {

    private transient Context mContext;
    private WebConfig mWebConfig;

    public WebNumberHandler(Context context, WebConfig webConfig) {
        mContext = context;
        mWebConfig = webConfig;
    }

    @Override
    public String url() {
        return mWebConfig.getUrl();
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public WebNumber find(String number) {
        return fetchDAta(mWebConfig, number);
    }

    @Override
    public boolean isOnline() {
        return false;
    }

    @Override
    public int getApiId() {
        return INumber.API_ID_WEB + mWebConfig.getIndex();
    }

    private WebNumber fetchDAta(WebConfig config, String num) {
        WebNumber webNumber = new WebNumber(num);

        Document doc = null;
        try {
            doc = Jsoup.connect(config.getUrl() + num)
                    .userAgent(config.getUa())
                    .get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HiLog.debug(Utils.TAG, doc.title());

        Elements elements = doc.select(config.getRoot());
        for (Element element : elements) {
            HiLog.debug(Utils.TAG, "%s\n\t%s".format(element.text(), ""));
        }

        Elements number = doc.select(config.getName());

        if (number.size() == 1) {
            HiLog.debug(Utils.TAG, "number: %s\n\t".format(number.get(0).text()));
            webNumber.setNumber(number.get(0).text());
        }

        Elements mark = doc.select(config.getMark());

        if (mark.size() == 1) {
            HiLog.debug(Utils.TAG, "mark: %s\n\t".format(mark.get(0).text()));
            webNumber.setName(mark.get(0).text());
        }

        Elements count = doc.select(config.getCount());

        if (count.size() == 1) {
            String c = count.get(0).text().replace("[^.0123456789]", "");
            HiLog.debug(Utils.TAG, "count: %s\n\t".format(c));
            webNumber.count = Integer.parseInt(c);
        }

        Elements name = doc.select(config.getName());

        if (name.size() == 1) {
            HiLog.debug(Utils.TAG, "name: %s\n\t".format(name.get(0).text()));
            webNumber.setName(name.get(0).text());
        }
        ;

        Elements address = doc.select(config.getAddress());

        if (address.size() == 1) {
            HiLog.debug(Utils.TAG, "address: %s\n\t".format(address.get(0).text()));
            webNumber.setProvince(address.get(0).text());
        }

        return webNumber;
    }
}
