package org.xdty.phone.number.model.common;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.model.NumberHandler;
import org.xdty.phone.number.model.caller.CallerNumber;
import org.xdty.phone.number.util.Utils;

import java.io.File;

public class CommonHandler implements NumberHandler<CommonNumber> {

    public final static String COMMON_VERSION_CODE_KEY = "common_db_version_code_key";
    public final static int COMMON_VERSION_CODE = 2;
    public final static String DB_NAME = "common.db";

    private Context mContext;

    public CommonHandler(Context context) {
        mContext = context.getApplicationContext();
        //checkVersion();
    }

    private void checkVersion() {
        DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
        Preferences pref = databaseHelper.getPreferences(mContext.getPreferencesDir().getName());
        if (pref.getInt(COMMON_VERSION_CODE_KEY, 0) < COMMON_VERSION_CODE) {
            Utils.removeCacheFile(mContext, DB_NAME);
        }
        pref.putInt(COMMON_VERSION_CODE_KEY, COMMON_VERSION_CODE);
    }

    @Override
    public String url() {
        return null;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public CommonNumber find(String number) {
        number = Utils.fixNumberPlus(number);
        CommonNumber commonNumber = null;
        RdbStore db = null;
        ResultSet cur = null;

        try {
            StoreConfig config = StoreConfig.newDefaultConfig(DB_NAME);
            DatabaseHelper helper = new DatabaseHelper(mContext);
            RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {

                @Override
                public void onCreate(RdbStore rdbStore) {
                    HiLog.info(Utils.TAG, "Create db" + rdbStore.toString());
                }

                @Override
                public void onUpgrade(RdbStore rdbStore, int i, int i1) {

                }
            };
            db = helper.getRdbStore(config, 1, rdbOpenCallback);
            cur = db.querySql("SELECT * FROM phone_number WHERE number = ? OR number = ? ",
                    new String[]{number, "0" + number});

            if (cur.getRowCount() == 1 && cur.goToFirstRow()) {
                String name = cur.getString(cur.getColumnIndexForName("name"));

                commonNumber = new CommonNumber(number, name);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (cur != null) {
                    cur.close();
                }
                if (db != null) {
                    db.close();
                }
            } catch (Exception e) {
                // ignore
            }
        }

        return commonNumber;
    }

    @Override
    public boolean isOnline() {
        return false;
    }

    @Override
    public int getApiId() {
        return INumber.API_ID_COMMON;
    }
}
