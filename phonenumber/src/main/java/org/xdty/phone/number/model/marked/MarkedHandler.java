package org.xdty.phone.number.model.marked;

import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.data.rdb.RdbOpenCallback;
import ohos.data.rdb.RdbStore;
import ohos.data.rdb.StoreConfig;
import ohos.data.resultset.ResultSet;
import ohos.hiviewdfx.HiLog;
import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.model.NumberHandler;
import org.xdty.phone.number.util.Utils;

import java.io.File;

public class MarkedHandler implements NumberHandler<MarkedNumber> {

    public final static String MARKED_VERSION_CODE_KEY = "marked_db_version_code_key";
    public final static int MARKED_VERSION_CODE = 1;
    public final static String DB_NAME = "marked.db";

    private Context mContext;

    public MarkedHandler(Context context) {
        mContext = context.getApplicationContext();
        //checkVersion();
    }

    private void checkVersion() {
        DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
        Preferences pref = databaseHelper.getPreferences(mContext.getPreferencesDir().getName());

        if (pref.getInt(MARKED_VERSION_CODE_KEY, 0) < MARKED_VERSION_CODE) {
            Utils.removeCacheFile(mContext, DB_NAME);
        }
        pref.putInt(MARKED_VERSION_CODE_KEY, MARKED_VERSION_CODE);
    }

    @Override
    public String url() {
        return null;
    }

    @Override
    public String key() {
        return null;
    }

    @Override
    public MarkedNumber find(String number) {
        number = Utils.fixNumberPlus(number);

        MarkedNumber markedNumber = null;
        RdbStore db = null;
        ResultSet cur = null;
        try {
            StoreConfig config = StoreConfig.newDefaultConfig(DB_NAME);
            DatabaseHelper helper = new DatabaseHelper(mContext);
            RdbOpenCallback rdbOpenCallback = new RdbOpenCallback() {

                @Override
                public void onCreate(RdbStore rdbStore) {
                    HiLog.info(Utils.TAG, "Create db" + rdbStore.toString());
                }

                @Override
                public void onUpgrade(RdbStore rdbStore, int i, int i1) {

                }
            };
            db = helper.getRdbStore(config, 1, rdbOpenCallback);
            cur = db.querySql("SELECT * FROM phone_number WHERE number = ? OR number = ? ",
                    new String[] { number });

            if (cur.getRowCount() == 0 && !number.startsWith("+") && !number.startsWith("0")) {
                cur.close();
                cur = db.querySql("SELECT * FROM phone_number WHERE number = ? OR number = ? ",
                        new String[] { "0" + number });
            }

            if (cur.getRowCount() >= 1 && cur.goToFirstRow()) {
                int type = cur.getInt(cur.getColumnIndexForName("type"));
                markedNumber = new MarkedNumber(number, type);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (db != null) {
                    db.close();
                }
                if (cur != null) {
                    cur.close();
                }
            } catch (Exception e) {
                // ignore
            }
        }

        return markedNumber;
    }

    @Override
    public boolean isOnline() {
        return false;
    }

    @Override
    public int getApiId() {
        return INumber.API_ID_MARKED;
    }
}
