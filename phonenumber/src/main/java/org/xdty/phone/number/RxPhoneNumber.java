package org.xdty.phone.number;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.openharmony.schedulers.OpenHarmonySchedulers;
import io.reactivex.rxjava3.parallel.ParallelFailureHandling;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.model.NumberHandler;
import org.xdty.phone.number.model.caller.CallerHandler;
import org.xdty.phone.number.model.caller.Status;
import org.xdty.phone.number.model.cloud.CloudNumber;
import org.xdty.phone.number.model.cloud.CloudService;
import org.xdty.phone.number.model.leancloud.LeanCloudHandler;
import org.xdty.phone.number.util.OkHttp;

import java.util.Collection;
import java.util.concurrent.Callable;
import okhttp3.OkHttpClient;
import org.xdty.phone.number.util.Utils;

public class RxPhoneNumber{
    public final static String API_TYPE = "api_type";
    private static final String TAG = RxPhoneNumber.class.getSimpleName();
    private Context mContext;
    private Preferences mPref;
    private CloudService mCloudService;

    public RxPhoneNumber() {
    }

    public RxPhoneNumber(Context context) {
        init(context);
    }

    public void init(Context context) {
        if (mContext == null) {
            mContext = context;
        }

        mPref = provideSharedPreferences();

        NumberProvider.init(mContext);

        OkHttpClient mOkHttpClient = OkHttp.get().client();

        mCloudService = new LeanCloudHandler(mContext, mOkHttpClient);
    }

    public CloudService getmCloudService() {
        return mCloudService;
    }

    public Preferences provideSharedPreferences() {
        DatabaseHelper databaseHelper = new DatabaseHelper(mContext);
        return databaseHelper.getPreferences(mContext.getPreferencesDir().getName());
    }

    public Flowable<INumber> getNumber(final String number) {
        return getNumber(number, NumberProvider.providers());
    }

    public Flowable<INumber> getOnlineNumber(final String number) {
        return getNumber(number, NumberProvider.providers(true));
    }

    public Flowable<INumber> getOfflineNumber(final String number) {
        return getNumber(number, NumberProvider.providers(false));
    }

    private Flowable<INumber> getNumber(final String number, Collection<NumberHandler> providers) {
        return Flowable.fromIterable(providers)
                .parallel(providers.size())
                .runOn(Schedulers.io())
                .map(new Function<NumberHandler, INumber>() {
                    @Override
                    public INumber apply(NumberHandler numberHandler) throws Exception {
                        HiLog.error(Utils.TAG, "apply: " + numberHandler);
                        try {
                            return numberHandler.find(number);
                        } catch (Exception | Error e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }, ParallelFailureHandling.SKIP)
                .sequential()
                .observeOn(OpenHarmonySchedulers.mainThread());
    }

    public Single<Boolean> put(final CloudNumber cloudNumber) {
        return Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return mCloudService.put(cloudNumber);
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(OpenHarmonySchedulers.mainThread());
    }

    public Single<Status> checkUpdate() {
        return Single.fromCallable(new Callable<Status>() {
            @Override
            public Status call() throws Exception {
                for (NumberHandler handler : NumberProvider.providers()) {
                    if (handler.isOnline() && handler.getApiId() == INumber.API_ID_CALLER) {
                        CallerHandler callerHandler = (CallerHandler) handler;
                        return callerHandler.checkUpdate();
                    }
                }
                return null;
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(OpenHarmonySchedulers.mainThread());
    }

    public Single<Boolean> upgradeData() {
        return Single.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                for (NumberHandler handler : NumberProvider.providers()) {
                    if (handler.isOnline() && handler.getApiId() == INumber.API_ID_CALLER) {
                        CallerHandler callerHandler = (CallerHandler) handler;
                        return callerHandler.upgradeData();
                    }
                }
                return null;
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(OpenHarmonySchedulers.mainThread());
    }
}