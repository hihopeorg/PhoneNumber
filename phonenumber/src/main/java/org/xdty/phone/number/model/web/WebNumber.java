package org.xdty.phone.number.model.web;

import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.model.Type;

public class WebNumber implements INumber {

    String number = "";
    String name = "";
    String province = "";
    String city = "";
    Type type = Type.NORMAL;
    int count = 0;

    public WebNumber(String number) {
        this.number = number;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getProvince() {
        return province;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String getCity() {
        return city;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public String getProvider() {
        return "";
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public boolean isOnline() {
        return true;
    }

    @Override
    public boolean hasGeo() {
        return true;
    }

    @Override
    public int getApiId() {
        return INumber.API_ID_WEB;
    }

    @Override
    public void patch(INumber i) {

    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" + getNumber() + ", " + getName() + ", " + getCount() + '}';
    }
}
