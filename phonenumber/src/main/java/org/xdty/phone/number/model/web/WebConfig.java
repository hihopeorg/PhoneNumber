package org.xdty.phone.number.model.web;

public class WebConfig {
    private String url;
    private String ua;
    private String root;
    private String number;
    private String name;
    private String mark;
    private String count;
    private String address;
    private int index;

    public WebConfig(String url, String ua, String root, String number, String name, String mark, String count, String address, int index) {
        this.url = url;
        this.ua = ua;
        this.root = root;
        this.number = number;
        this.name = name;
        this.mark = mark;
        this.count = count;
        this.address = address;
        this.index = index;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUa() {
        return ua;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
