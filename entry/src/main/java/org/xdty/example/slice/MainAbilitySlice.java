package org.xdty.example.slice;

import io.reactivex.rxjava3.functions.Consumer;
import ohos.agp.components.Text;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.hiviewdfx.HiLog;
import org.xdty.example.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import org.xdty.phone.number.RxPhoneNumber;
import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.model.cloud.CloudNumber;
import org.xdty.phone.number.util.Utils;

public class MainAbilitySlice extends AbilitySlice {

    private RxPhoneNumber mRxPhoneNumber;
    private Text textView;
    private String result = "";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        mRxPhoneNumber = new RxPhoneNumber();
        mRxPhoneNumber.init(this);
        textView = (Text) findComponentById(ResourceTable.Id_text);

        // set prefer api
        CloudNumber cloudNumber = new CloudNumber();
        cloudNumber.setUid("dadasdasfadsfsad");
        cloudNumber.setCount(0);
        cloudNumber.setFrom(1);
        cloudNumber.setType(1);
        cloudNumber.setName("骚扰");
        cloudNumber.setNumber("1222033");

        // set prefer api
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        Preferences pref = databaseHelper.getPreferences(getPreferencesDir().getName());
        pref.putInt("api_type", INumber.API_ID_SG);

        final long start = System.currentTimeMillis();
        mRxPhoneNumber.getNumber("95194049").subscribe(new Consumer<INumber>() {
            @Override
            public void accept(INumber number) throws Exception {
                try {
                    String s = "onResponse: " +
                            number.getNumber() +
                            ": " +
                            number.getType().getText() +
                            ", " +
                            number.getName() +
                            ", " +
                            number.getCount() +
                            " : " +
                            number.getProvince() +
                            " " +
                            number.getCity() +
                            " " +
                            number.getProvider() +
                            ", " +
                            number.getApiId();
                    result += s + "\n";
                    textView.setText(result);
                    HiLog.error(Utils.TAG, (System.currentTimeMillis() - start) + ": " + s);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
