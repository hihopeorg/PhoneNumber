package org.xdty.example;

import io.reactivex.rxjava3.functions.Consumer;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Text;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.media.image.PixelMap;
import ohos.media.image.common.AlphaType;
import ohos.media.image.common.Size;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xdty.phone.number.RxPhoneNumber;
import org.xdty.phone.number.model.INumber;
import org.xdty.phone.number.util.Utils;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    private RxPhoneNumber mRxPhoneNumber;
    private Context context;

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("org.xdty.example", actualBundleName);
    }


    @Before
    public void setUp() throws Exception {
        mRxPhoneNumber = new RxPhoneNumber();
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        mRxPhoneNumber.init(context);

    }

    @Test
    public void setup_1() {
        Assert.assertNotNull("It is ok", mRxPhoneNumber.getmCloudService());
    }

    @Test
    public void setup_2() {
        Assert.assertNotNull("It is ok", mRxPhoneNumber.provideSharedPreferences());
    }

    @Test
    public void setup_3() {
        Assert.assertNotNull("It is ok", mRxPhoneNumber.getNumber("95194049"));
    }

    @Test
    public void setup_4() {
        mRxPhoneNumber.getNumber("95194049").subscribe(new Consumer<INumber>() {
            @Override
            public void accept(INumber number) throws Exception {
                try {
                    Assert.assertEquals("It is ok", number.getApiId(), 16);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}